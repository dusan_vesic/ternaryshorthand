<?php

namespace Me;

class Demo 
{
    private $args;
    
    function __construct(array $args) 
    {
        $this->args = $args;
    }
    
    function get()
    {
        return $this->args;
    }
}