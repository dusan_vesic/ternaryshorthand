# Ternary Shorthand

### About
This should prove that if:

`$a = isset($foo) ? $foo : 'default';`

and

`$b = $foo ?: 'default'`

$a and $b are equivalent.

### Usage
`
git clone
`

`
composer install
`

`
vendor/bin/phpunit src/DemoTest.php
`
