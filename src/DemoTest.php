<?php

use PHPUnit\Framework\TestCase;
use Me\Demo;

class DemoTest extends TestCase
{
    private $data;
    
    public function setUp() 
    {
        $this->data = [
            'id'    => 1,
            'name'  => 'alice'
        ];
    }
    
    public function testKeyExists()
    {
        $demo = new Demo($this->data);
        $args = $demo->get();
        
        $left   = isset($args['id'])  ?  $args['id'] : 0;
        $right  = $args['id']         ?: 0;
        
        $this->assertSame($left, $right);
        $this->assertSame(1, $left);
        $this->assertSame(1, $right);
        $this->assertNotSame(0, $left);
        $this->assertNotSame(0, $right);
        $this->assertNotSame(null, $left);
        $this->assertNotSame(null, $right);
        
        $left   = isset($args['name'])  ? $args['name'] : 'default';
        $right  = $args['name']         ?: 'default';
        
        $this->assertSame($left, $right);
        $this->assertSame('alice', $left);
        $this->assertSame('alice', $right);
        $this->assertNotSame('default', $left);
        $this->assertNotSame('default', $right);
        $this->assertNotSame(null, $left);
        $this->assertNotSame(null, $right);
    }
    
    public function testKeyNotExists()
    {
        $demo = new Demo($this->data);
        $args = $demo->get();
        
        $left   = isset($args['foo'])  ?  $args['foo'] : null;
        $right  = $args['foo']         ?: null;
                
        $this->assertSame($left, $right);
        $this->assertSame(null, $left);
        $this->assertSame(null, $right);
        $this->assertNotSame(0, $left);
        $this->assertNotSame(0, $right);
        $this->assertNotSame('', $left);
        $this->assertNotSame('', $right);
    }
}